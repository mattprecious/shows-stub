package com.example.shows;

import android.app.Application;
import dagger.ObjectGraph;
import timber.log.Timber;

public final class ShowsApp extends Application {
  private ObjectGraph globalGraph;

  @Override public void onCreate() {
    super.onCreate();

    if (BuildConfig.DEBUG) {
      Timber.plant(new Timber.DebugTree());
    }

    buildObjectGraph();
  }

  public void buildObjectGraph() {
    globalGraph = ObjectGraph.create(Modules.list(this));
    globalGraph.inject(this);
  }

  public ObjectGraph getGlobalGraph() {
    return globalGraph;
  }

  public void inject(Object target) {
    globalGraph.inject(target);
  }
}
