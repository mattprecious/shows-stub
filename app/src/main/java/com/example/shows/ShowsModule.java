package com.example.shows;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import com.example.shows.data.MockRequestHandler;
import com.example.shows.data.MockShowsService;
import com.example.shows.data.ShowsService;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.Picasso;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import retrofit.MockRestAdapter;
import retrofit.RestAdapter;
import retrofit.android.AndroidMockValuePersistence;
import retrofit.client.OkClient;

@Module(
    injects = {
        ShowsApp.class, MainActivity.class,
    },
    library = true) public final class ShowsModule {
  private final ShowsApp app;

  public ShowsModule(ShowsApp app) {
    this.app = app;
  }

  @Provides @Singleton Application provideApplication() {
    return app;
  }

  @Provides @Singleton SharedPreferences provideSharedPreferneces(Application application) {
    return application.getSharedPreferences("shows", Context.MODE_PRIVATE);
  }

  @Provides @Singleton Picasso providePicasso(Application application,
      MockRestAdapter mockRestAdapter) {
    return new Picasso.Builder(application) //
        .addRequestHandler(new MockRequestHandler(mockRestAdapter, application.getAssets())) //
        .build();
  }

  @Provides @Singleton OkHttpClient provideOkHttpClient() {
    return new OkHttpClient();
  }

  @Provides @Singleton RestAdapter provideRestAdapter(OkHttpClient client) {
    return new RestAdapter.Builder() //
        .setClient(new OkClient(client)) //
        .setEndpoint("http://example.com") //
        .build();
  }

  @Provides @Singleton MockRestAdapter provideMockRestAdapter(RestAdapter restAdapter,
      SharedPreferences preferences) {
    MockRestAdapter mockRestAdapter = MockRestAdapter.from(restAdapter);
    AndroidMockValuePersistence.install(mockRestAdapter, preferences);
    return mockRestAdapter;
  }

  @Provides @Singleton ShowsService createAppService(MockRestAdapter mockRestAdapter,
      MockShowsService mockService) {
    return mockRestAdapter.create(ShowsService.class, mockService);
  }
}
