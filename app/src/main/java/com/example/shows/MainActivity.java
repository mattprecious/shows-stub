package com.example.shows;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.example.shows.data.Shows;
import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {
  @Inject Shows shows;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    ((ShowsApp) getApplication()).getGlobalGraph().inject(this); // Required setup.

    setContentView(R.layout.main);
  }
}
