package com.example.shows;

final class Modules {
  static Object[] list(ShowsApp app) {
    return new Object[] {
        new ShowsModule(app),
    };
  }
}
