package com.example.shows.data;

import java.util.List;
import retrofit.Callback;
import retrofit.http.GET;
import rx.Observable;

public interface ShowsService {
  @GET("/1.0/shows") //
  void shows(Callback<List<Show>> callback);

  @GET("/1.0/shows") //
  Observable<List<Show>> shows();
}
