package com.example.shows.data;

import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;
import retrofit.Callback;
import rx.Observable;

@Singleton
public final class MockShowsService implements ShowsService {
  @Inject public MockShowsService() {
  }

  @Override public void shows(Callback<List<Show>> callback) {
    callback.success(Shows.SHOWS, null);
  }

  @Override public Observable<List<Show>> shows() {
    return Observable.just(Shows.SHOWS);
  }
}
