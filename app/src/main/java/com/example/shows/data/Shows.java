package com.example.shows.data;

import java.util.Arrays;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton public final class Shows {
  public static final List<Show> SHOWS = Arrays.asList( //
      new Show.Builder() //
          .name("Marvel's Daredevil") //
          .description(
              "Blinded as a young boy, Matt Murdock fights injustice by day as a lawyer and by night as the Super Hero Daredevil in New York City. Matt Murdock, who in a tragic accident was blinded as a boy but imbued with extraordinary senses. Murdock sets up practice in his old neighborhood of Hell's Kitchen, New York where he now fights against injustice as a respected lawyer by day and masked vigilante at night.") //
          .startDate(1428624000000L) //
          .thumbnailUrl("mock:///daredevil_thumb.jpg") //
          .build(), //
      new Show.Builder() //
          .name("Doctor Who") //
          .description(
              "The Doctor is an alien Time Lord from the planet Gallifrey who travels through all of time and space in his TARDIS. He has a long list of friends and companions who have shared journeys with him. Instead of dying, the Doctor is able to “regenerate” into a new body, taking on a new personality with each regeneration. Twelve actors, plus John Hurt, have played The Doctor thus far.") //
          .startDate(1111795200000L) //
          .thumbnailUrl(null) //
          .build(), //
      new Show.Builder() //
          .name("Sherlock") //
          .description("Sherlock is a British television crime drama that presents a contemporary adaptation of Sir Arthur Conan Doyle's Sherlock Holmes detective stories. Created by Steven Moffat and Mark Gatiss, it stars Benedict Cumberbatch as Sherlock Holmes and Martin Freeman as Doctor John Watson.") //
          .startDate(1280016000000L) //
          .thumbnailUrl(null) //
          .build(), //
      new Show.Builder() //
          .name("Marvel's Jessica Jones") //
          .description("Ever since her short-lived stint as a Super Hero ended in tragedy, Jessica Jones has been rebuilding her personal life and career as a hot-tempered, sardonic, badass private detective in Hell's Kitchen, New York City. Plagued by self-loathing, and a wicked case of PTSD, Jessica battles demons from within and without, using her extraordinary abilities as an unlikely champion for those in need... especially if they're willing to cut her a check.") //
          .startDate(1447977600000L) //
          .thumbnailUrl("mock:///jessicajones_thumb.jpg") //
          .build(), //
      new Show.Builder() //
          .name("Community") //
          .description("From Emmy Award-winner Dan Harmon comes \"Community\", a smart comedy series about higher education – and lower expectations. The student body at Greendale Community College is made up of high-school losers, newly divorced housewives, and old people who want to keep their minds active. Within these not-so-hallowed halls, Community focuses on a band of misfits, at the center of which is a fast-talking lawyer whose degree was found to be fake, who form a study group and end up learning a lot more about themselves than they do about their course work.\n") //
          .startDate(1253145600000L) //
          .thumbnailUrl("mock:///community_thumb.jpg") //
          .build(), //
      new Show.Builder() //
          .name("Futurama") //
          .description("A late 20th-century New York City pizza delivery boy, Philip J. Fry, after being unwittingly cryogenically frozen for one thousand years, finds employment at Planet Express, an interplanetary delivery company in the 31st century.") //
          .startDate(922579200000L) //
          .thumbnailUrl("mock:///futurama_thumb.jpg") //
          .build(), //
      new Show.Builder() //
          .name("Scrubs") //
          .description("Scrubs focuses on the lives of several people working at Sacred Heart, a teaching hospital. It features fast-paced dialogue, slapstick, and surreal vignettes presented mostly as the daydreams of the central character, Dr. John Michael \"J.D.\" Dorian.") //
          .startDate(1001980800000L) //
          .thumbnailUrl("mock:///scrubs_thumb.jpg") //
          .build(), //
      new Show.Builder() //
          .name("Black Mirror") //
          .description("Over the last ten years, technology has transformed almost every aspect of our lives before we've had time to stop and question it. In every home; on every desk; in every palm - a plasma screen; a monitor; a smartphone - a black mirror of our 21st Century existence. Black Mirror is a contemporary British re-working of The Twilight Zone with stories that tap into the collective unease about our modern world.") //
          .startDate(1322956800000L) //
          .thumbnailUrl(null) //
          .build(), //
      new Show.Builder() //
          .name("Brooklyn Nine-Nine") //
          .description("A single-camera ensemble comedy following the lives of an eclectic group of detectives in a New York precinct, including one slacker who is forced to shape up when he gets a new boss.") //
          .startDate(1379376000000L) //
          .thumbnailUrl(null) //
          .build(), //
      new Show.Builder() //
          .name("Archer") //
          .description("At ISIS, an international spy agency, global crises are merely opportunities for its highly trained employees to confuse, undermine, betray and royally screw each other. At the center of it all is suave master spy Sterling Archer, whose less-than-masculine code name is \"Duchess.\" Archer works with his domineering mother Malory, who is also his boss. Drama revolves around Archer's ex-girlfriend, Agent Lana Kane and her new boyfriend, ISIS comptroller Cyril Figgis, as well as Malory's lovesick secretary, Cheryl.") //
          .startDate(1253145600000L) //
          .thumbnailUrl(null) //
          .build());

  @Inject public Shows() {
  }

  public List<Show> shows() {
    return SHOWS;
  }
}
