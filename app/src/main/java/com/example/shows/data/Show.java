package com.example.shows.data;

import android.support.annotation.Nullable;

public final class Show {
  public final String name;
  @Nullable public final String description;
  public final long startDate;
  @Nullable public final String thumbnailUrl;

  private Show(Builder builder) {
    name = builder.name;
    description = builder.description;
    startDate = builder.startDate;
    thumbnailUrl = builder.thumbnailUrl;
  }

  @Override public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Show)) return false;

    Show show = (Show) o;

    if (startDate != show.startDate) return false;
    if (!name.equals(show.name)) return false;
    if (description != null ? !description.equals(show.description) : show.description != null) {
      return false;
    }
    return !(thumbnailUrl != null ? !thumbnailUrl.equals(show.thumbnailUrl)
        : show.thumbnailUrl != null);
  }

  @Override public int hashCode() {
    int result = name.hashCode();
    result = 31 * result + (description != null ? description.hashCode() : 0);
    result = 31 * result + (int) (startDate ^ (startDate >>> 32));
    result = 31 * result + (thumbnailUrl != null ? thumbnailUrl.hashCode() : 0);
    return result;
  }

  @Override public String toString() {
    return "Show{" +
        "name='" + name + '\'' +
        ", description='" + description + '\'' +
        ", startDate=" + startDate +
        ", thumbnailUrl='" + thumbnailUrl + '\'' +
        '}';
  }

  static final class Builder {
    public String name;
    public String description;
    public long startDate;
    public String thumbnailUrl;

    public Builder name(String name) {
      this.name = name;
      return this;
    }

    public Builder description(@Nullable String description) {
      this.description = description;
      return this;
    }

    public Builder startDate(long startDate) {
      this.startDate = startDate;
      return this;
    }

    public Builder thumbnailUrl(@Nullable String thumbnailUrl) {
      this.thumbnailUrl = thumbnailUrl;
      return this;
    }

    public Show build() {
      return new Show(this);
    }
  }
}
